#pragma once
#include "shape.h"
#include <iostream>


class Pentagon:public Shape
{
public :
	Pentagon(std::string name, std::string col, double rib);
	void setRib(double rib);
	double getRib();
	void draw();

private:
	double _rib;
};
