#pragma once
#include "shape.h"
#include <iostream>


class Hexgon :public Shape
{
public:
	Hexgon(std::string name, std::string col, double rib);
	void setRib(double rib);
	double getRib();
	void draw();

private:
	double _rib;
};
