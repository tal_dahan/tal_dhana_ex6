#include "MathUtils.h"
#include <iostream>


double MatUtils::CalPentagonArea(double rib)
{
	// Formula to find Pentagon area 
	double area = (sqrt(5 * (5 + 2 * (sqrt(5)))) * rib * rib) / 4;

	return area;
}

double MatUtils::CalHexagonArea(double rib)
{
	// Formula to find Hexagon area 
	return ((3 * sqrt(3) *
		(rib * rib)) / 2);
}