#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"
//including the new classes
#include "Pentagon.h"
#include "MathUtils.h"
#include "Hexagon.h"

using namespace std;
int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0;
	double rib = 5;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;


	Pentagon p(nam, col, rib);
	Shape *ptrPen = &p;

	Hexgon h(nam, col, rib);
	Shape *ptrhex = &h;

	
	
	std::cout << "Enter information for your objects" << std::endl;
	string isChar; // add stringto check input validation off choose
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "\nwhich shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, t = pentagon , x = hexagon" << std::endl;
		std::cin >> isChar;
		//if the usser enter more than 1 char 
		if (isChar.length() > 1)
		{
			shapetype = 'g';//set shapetype as invalid value
		}
		else
		{
			shapetype = isChar[0];//set shapetypea the first char 
		}
		
try
		{

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				if (!rad)//check if the user insert string  insted of int  
				{
					throw InputException();
				}
				else if (rad < 0)//check validation of radius
				{
					throw shapeException();
				}

				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (!height||!width)//check if the user insert string  insted of int  
				{
					throw InputException();
				}
				else if (height<= 0 || width<= 0 )
				{
					throw shapeException();

				}
					quad.setName(nam);
					quad.setColor(col);
					quad.setHeight(height);
					quad.setWidth(width);
					ptrquad->draw();
				
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (!height || !width)//check if the user insert string  insted of int  
				{
					throw InputException();
				}
				else if (height <= 0 || width <= 0)//check validation of radius
				{
					throw shapeException();
				}
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				//check validation of ang	
				if (!height || !width||!ang ||!ang2)//check if the user insert string  insted of int  
				{
					throw InputException();
				}
				else if (ang+ang2 != 180 || height <= 0 || width <= 0 ||ang <= 0 ||ang2 <= 0 )
				{
					throw shapeException();
				}
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
			case 't'://pentagin case
				std::cout << "enter color, name,  rib for pentagon" << std::endl;
				std::cin >> col >> nam >> rib;
				if (!rib)//check if the user insert string  insted of int  
				{
					throw InputException();
				}
				else if (rib < 0)//check validation of rib
				{
					throw shapeException();
				}
				//set the values
				p.setColor(col);
				p.setName(nam);
				p.setRib(rib);
				ptrPen->draw();
				break;
			case 'x'://hexagon case
				std::cout << "enter color, name,  rib for hexagon" << std::endl;
				std::cin >> col >> nam >> rib;
				if (!rib)//check if the user insert string  insted of int  
				{
					throw InputException();
				}
				else if (rib < 0)//check validation of rib
				{
					throw shapeException();
				}
				//set the values
				h.setColor(col);
				h.setName(nam);
				h.setRib(rib);
				ptrhex->draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			//check validation
			if (isChar.length() ==  1)
			{
				std::cout << "\nwould you like to add more object press any key if not press x" << std::endl;
				std::cin >> x;//fix the get 
			}
			
		}
		catch (InputException & e)
		{
			printf(e.what());
			cin.clear();
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		catch (std::exception& e)
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}