#include "Pentagon.h"
#include "MathUtils.h"



void Pentagon::draw()
{
	MatUtils a;//creat MathUtils varible to use the area function
	std::cout << getName() << std::endl << getColor() << std::endl << "rib is " << getRib()<< std::endl << "Area is " << a.CalPentagonArea(getRib()) ;

}
Pentagon::Pentagon(std::string nam, std::string col, double rib) :Shape(col, nam)
{
	setRib(rib);
}

void Pentagon::setRib(double rib)
{
	this->_rib = rib;
}

double Pentagon::getRib()
{
	return this->_rib;
}