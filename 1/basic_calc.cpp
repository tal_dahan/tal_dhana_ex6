#include <iostream>
#define ERROR 0
#define EROOR_MSG "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."
using namespace std;

int add(int a, int b,bool &isValid) {
	//check validation of the numbers and the sum
	if ( a+b == 8200)
	{
		cerr << EROOR_MSG << endl;//print the error
		isValid = ERROR;
	}
	return a + b;
}

int  multiply(int a, int b, bool &isValid) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a, isValid);
	//check validation
	if (!isValid)
	{
		return ERROR;
	}
 };
  
  return sum;
}

int  pow(int a, int b,bool &isValid) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a, isValid);
  };
  return exponent;
}

int main(void){
	bool isValid =1 ;
	int mul = 0;
	int p = 0;
	/*check add*/
	int Ad = add(10, 200, isValid);//valid
	//check if valid -  print the result
	if (isValid)
	{
		cout << Ad << endl;
	}
	/*mul*/
	isValid = 1; 
	mul = multiply(200, 200, isValid);//invalid
	//check if valid -  print the result
	if (isValid)
	{
		cout << mul << endl;
	}

	/*pow*/
	isValid = 1;
	p = pow(100,10, isValid);//invalid
	//check if valid -  print the result
	if (isValid)
	{
		cout << p << endl;
	}

	getchar();
}