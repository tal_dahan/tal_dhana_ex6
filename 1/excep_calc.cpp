#include <iostream>
#define ERROR 0
#define EROOR_MSG "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."
using namespace std;

int add(int a, int b) {
	//trow back if the sum is 8200
	if (a + b == 8200)
	{
		throw ERROR;
	}

    return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
	if (sum == 8200)//trow back if the sum is 8200
	{
		throw ERROR;
	}
  };
  return sum;
}

int  pow(int a, int b) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
  };
  return exponent;
}

int main(void) {
	try 
	{
		int Ad = add(8000, 200);//InValid 
		cout << Ad << endl;//print the result
	}
	catch (int a)
	{
		cerr << EROOR_MSG << endl;//error msg if the sum is  8200
	}


	try
	{
		int sum = multiply(201, 200);//valid 
		cout << sum << endl;//print the result
	}
	catch (int a)
	{
		cerr << EROOR_MSG << endl;//error msg if the sum is  8200
	}

	try
	{
		int p = pow(100, 2);//InValid 
		cout << p << endl;//print the result
	}
	catch (int a)
	{
		cerr << EROOR_MSG << endl;//error msg if the sum is  8200
	}
	
	getchar();

}